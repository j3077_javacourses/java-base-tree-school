import java.util.*;

public class Registro {
    HashSet<Studente> studenti = new HashSet<>();
    HashSet<Corso> corsi = new HashSet<>();
    HashSet<Esame> esami = new HashSet<>();


    public void addStudent(Studente s) {
        studenti.add(s);
    }

    public void addCorso(Corso c) {
        corsi.add(c);
    }

    public void addEsami(Esame e) {
        esami.add(e);
    }

    //lista degli studenti con età compresa tra 23 e 27 anni
    public ArrayList<Studente> getStudents23to27() {
        ArrayList<Studente> students23to27 = new ArrayList<>();
        for (Studente s : studenti) {
            if (s.getEtà() >= 23 && s.getEtà() <= 27) {
                students23to27.add(s);
            }
        }
        return students23to27;
    }

    //lista delle medie degli studenti di età compresa tra 23 e 27 anni
    public ArrayList<Integer> getAverageStudents23to27() {
        ArrayList<Integer> average = new ArrayList<>();
        for (Studente s : getStudents23to27()) {
            average.add(getMediaAritmetica(s));
        }
        return average;
    }

    // trova gli studenti che hanno età inferiore a 25 e ne stampa
    // nome cognome e media dei voti
    public void studentsUnder25() {
        for (Studente s : studenti) {
            if (s.getEtà() < 25) {
                System.out.println(s.getNome() + " " + s.getCognome() + " Media Voti: " + getMediaAritmetica(s));
            }
        }
    }

    // restituisce la media aritmetica dello studente
    public int getMediaAritmetica(Studente s) {
        int somma_voti = 0;
        int tot_voti = 0;
        for (Esame e : esami) {
            if (e.getStudente().equals(s)) {
                somma_voti += e.getVoto();
                tot_voti++;
            }
        }
        //ritorna 0 se non è stato sostenuto nessun esame
        try {
            return somma_voti / tot_voti;
        } catch (ArithmeticException e) {
            return 0;
        }
    }

    // ritorna una lista di studenti che hanno età compresa tra 27 e 30 anni
    // e che hanno una media superiore al 28
    public ArrayList<Studente> getStudents27to30AverageMin28() {
        ArrayList<Studente> listaStudenti = new ArrayList<>();
        for (Studente s : studenti) {
            if (s.getEtà() >= 27 && s.getEtà() <= 30) {
                if (getMediaAritmetica(s) > 28) {
                    listaStudenti.add(s);
                }
            }
        }
        return listaStudenti;
    }

    //restituisce la media ponderata dello studente
    public int getMediaPonderata(Studente s) {
        int media_num = 0;
        int sum_crediti = 0;
        for (Esame e : esami) {
            if (e.getStudente().equals(s)) {
                media_num += e.getCorso().getCrediti() * e.getVoto();
                sum_crediti += e.getCorso().getCrediti();
            }
        }
        return media_num / sum_crediti;
    }

    //restituisce una lista con tutti gli studenti ordinati in base all'età
    public ArrayList<Studente> getStudenti() {
        ArrayList<Studente> studenti = new ArrayList<>();
        for (Studente s : this.studenti) {
            studenti.add(s);
        }
        Collections.sort(studenti, (o1, o2) -> o1.getEtà() < o2.getEtà() ? -1 : o1.getEtà() == o2.getEtà() ? 0 : 1);
        {
            return studenti;
        }
    }

    //restituisce la media di tutti gli studenti
    public void getMedie() {
        for (Studente s : studenti) {
            System.out.println(s.getNome() + " " + s.getCognome() + ", Media: " + getMediaAritmetica(s));
        }
    }

    //media degli studenti in ordine crescente
    public void getMedieOrdinateCrescente() {
        ArrayList<Studente> mediaOrdinata = new ArrayList<>();
        for (Studente s : this.studenti) {
            mediaOrdinata.add(s);
        }
        Collections.sort(mediaOrdinata, (o1, o2) -> getMediaAritmetica(o1) < getMediaAritmetica(o2) ? -1 : getMediaAritmetica(o1) == getMediaAritmetica(o2) ? 0 : 1);
        for (Studente s : mediaOrdinata) {
            System.out.println((s.getNome() + " " + s.getCognome() + ", Media: " + getMediaAritmetica(s)));
        }
    }

    //media degli studenti in ordine descrescente
    public ArrayList<Studente> getMedieOrdinateDecrescente() {
        ArrayList<Studente> mediaOrdinata = new ArrayList<>();
        for (Studente s : this.studenti) {
            mediaOrdinata.add(s);
        }
        Collections.sort(mediaOrdinata, (o1, o2) -> getMediaAritmetica(o1) < getMediaAritmetica(o2) ? 1 : getMediaAritmetica(o1) == getMediaAritmetica(o2) ? 0 : -1);
      /*  for (Studente s : mediaOrdinata) {
            System.out.println((s.getNome() + " " + s.getCognome() + ", Media: " + getMediaAritmetica(s)));
        }*/
        return mediaOrdinata;
    }

    //sstampa i dati dello studente con la media più alta
    // insieme ai nomi dei corsi, crediti e voti degli esami
    public void getBestStudent() {
        ArrayList<Studente> mediaOrdinata = getMedieOrdinateDecrescente();
        Studente bestStudent = mediaOrdinata.get(0);
        for (Esame e : esami) {
            if (e.getStudente().equals(bestStudent)) {
                System.out.println(e);

            }
        }
    }

}
