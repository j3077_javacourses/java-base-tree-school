public class Esame {
    private Studente studente;
    private Corso corso;
    private int voto;
    private boolean registered = false;


    public Esame(Studente studente, Corso corso) {
        this.studente = studente;
        this.corso = corso;
    }

    public Studente getStudente() {
        return studente;
    }

    public int getVoto() {
        return voto;
    }

    public Corso getCorso() {
        return corso;
    }

    public boolean isRegistered() {
        return registered;
    }

    //prende in ingresso un voto e se non è ancora stato
    // registrato lo sovrascrive
    public void sostieni(int voto) {
        if (!registered) {
            this.voto = voto;
        }
    }

    //se il voto è sufficiente indica l'esame come registrato
    public void registra() {
        if (voto >= 18) {
            this.voto = voto;
            registered = true;
        }

    }

    @Override
    public String toString() {
        return studente +
                ", corso: " + corso +
                ", voto: " + voto;
    }
}
