
import java.util.Objects;

public class Studente extends Persona {
    private String università;
    private int matricola;

    public Studente(String nome, String cognome, int età, String università, int matricola) {
        super(nome, cognome, età);
        this.università = università;
        this.matricola = matricola;
    }


    public String getUniversità() {
        return università;
    }

    public int getMatricola() {
        return matricola;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Studente studente = (Studente) o;
        return matricola == studente.matricola;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), matricola);
    }

    @Override
    public String toString() {
        return super.getNome() + " " + super.getCognome() + ", Matricola: " + matricola;
    }

}
