public class Main {
    public static void main(String[] args) {
        Studente mario = new Studente("Mario", "Rossi", 30, "politecnico", 123456);
        Studente andrea = new Studente ("Andrea", "Bianchi", 30, "ecampus", 148937);
        Studente carlo = new Studente("Carlo", "Gorla", 28, "ecampus", 1859372);
        Studente maria = new Studente("Maria", "Bianchi", 32, "eCampus", 1238473);

        Corso fisica  = new Corso("elettromagnetismo", 10);
        Corso analisi = new Corso("analisi", 5);
        Corso meccanica = new Corso("meccanica", 15);

        Esame fisicaCarlo = new Esame(carlo, fisica);
        Esame analisiCarlo = new Esame(carlo, analisi);

        Esame fisicaMario = new Esame(mario, fisica);
        Esame analisiMario = new Esame(mario, analisi);
        Esame meccanicaMario = new Esame(mario, meccanica);

        Esame fisicaAndrea = new Esame(andrea, fisica);
        Esame analisiAndrea = new Esame(andrea, analisi);
        Esame meccanicaAndrea = new Esame(andrea, meccanica);

        Registro registro = new Registro();
        registro.addStudent(mario);
        registro.addStudent(andrea);
        registro.addStudent(maria);
        registro.addStudent(carlo);
        registro.addCorso(fisica);
        registro.addCorso(analisi);
        registro.addEsami(fisicaCarlo);
        registro.addEsami(analisiCarlo);
        registro.addEsami(fisicaMario);
        registro.addEsami(analisiMario);
        registro.addEsami(meccanicaMario);
        registro.addCorso(meccanica);

        registro.addEsami(fisicaAndrea);
        registro.addEsami(analisiAndrea);
        registro.addEsami(meccanicaAndrea);

        fisicaAndrea.sostieni(18);
        analisiAndrea.sostieni(15);
        meccanicaAndrea.sostieni(18);
        fisicaAndrea.registra();
        analisiAndrea.registra();
        meccanicaAndrea.registra();


        fisicaCarlo.sostieni(16);
        analisiCarlo.sostieni(17);
        fisicaCarlo.registra();
        analisiCarlo.registra();

        fisicaMario.sostieni(28);
        analisiMario.sostieni(30);
        meccanicaMario.sostieni(26);
        fisicaMario.registra();
        analisiMario.registra();
        meccanicaMario.registra();




       // System.out.println(registro.getAverageStudents23to27());
       // System.out.println(registro.getStudents27to30AverageMin28());
       // System.out.println(registro.getMediaPonderata(mario));

        //System.out.println(registro.getStudenti());
        //registro.getMedie();
        //registro.getMedieOrdinateCrescente();
        //registro.getMedieOrdinateDecrescente();

        registro.getBestStudent();

    }
}
