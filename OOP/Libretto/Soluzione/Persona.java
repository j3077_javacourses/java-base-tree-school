import java.util.Objects;

public class Persona {
    private String nome;
    private String cognome;
    private int età;
    private final String CODICE_FISCALE;


    public Persona(String nome, String cognome, int età) {
        this.nome = nome;
        this.cognome = cognome;
        this.età = età;
        this.CODICE_FISCALE = nome + cognome + età;

    }

    public String getNome() {
        return nome;
    }

    public String getCognome() {
        return cognome;
    }

    public int getEtà() {
        return età;
    }

    public String getCODICE_FISCALE() {
        return CODICE_FISCALE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Persona persona = (Persona) o;
        return CODICE_FISCALE.equals(persona.CODICE_FISCALE);
    }

    @Override
    public int hashCode() {
        return Objects.hash(CODICE_FISCALE);
    }


}
