# Libretto

Creare la classe **Persona**, caratterizzata da nome, cognome, età, codice fiscale:
due persone sono uguali se hanno stesso codice fiscale
Creare la classe Studente che estende Persona:
- uno studente è ora caratterizzato anche dal codice studente e da una stringa che
indica l’università a cui è iscritto
- Due studenti sono uguali se hanno stesso codice studente

Creare la classe **Corso** caratterizzata da nome e numero di crediti

Creare una classe **Esame**:
- E’ caratterizzata dagli attributi studente, corso, voto (un numero intero), e
infine un attributo che indica se il voto è stato registrato o meno
- Il costruttore imposta solo i primi due parametri
- Il metodo sostieni prende in ingresso un voto e, se l’esame non è stato
ancora registrato, sovrascrive il voto
- Il metodo registra non prede alcun parametro e, se l’esame ha una
valutazione sufficiente, indica l’esame come registrato (non sarà più
possibile la modifica della valutazione)

Aggiungere una classe **Registro** caratterizzata da un set di studenti, un set di
corsi e una set di esami. Implementare i seguenti metodi:
- Creare un metodo void per trovare gli studenti che hanno età inferiore di 25
anni (mostrare nome e media di questi studenti! Alla faccia della privacy)
- Creare un metodo che renda una lista con tutti gli studenti che hanno una
media superiore al 27
- Creare un metodo che renda una lista media di tutti gli studenti che hanno
un’età compresa tra 23 e 27 anni
- Creare un metodo che renda una lista gli studenti che hanno un’età compresa
tra 27 e 30 anni e che abbiano una media superiore al 28.

## Metodi Avanzati
- *getMediaAritmetica* dato uno studente rende la sua media aritmetica
- *getMediaPonderata* dato uno studente rende la sua media ponderata
- *getStudenti* stampa la lista di studenti ordinati per età
- *getMedie* stampa a video un report delle medie di tutti gli studenti:
opzionalmente il metodo può stampargli in ordine crescente o decrescente
- *getBestStudente* stampa a video lo studente con la media più alta,
visualizzando i suoi attributi e l’elenco dei suoi esami (visualizzando nome del
corso, numero di crediti e il voto ottenuto)