
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;


public class CopyFile {
    public static void main(String[] args) throws IOException {
        ArrayList<String> readLines = new ArrayList<>();

        try {
            BufferedReader reader = Files.newBufferedReader(Path.of("Streams/CopiaFile/LoremIpsum.txt"));

            //scorre tutte le righe del file reader e le aggiunge alla lista readLines.
            //ogni riga è un elemento della lista
            String line;
            while ((line = reader.readLine()) != null) {
                readLines.add(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        //vado infine a scrivere sul file "copia.txt" prendendo le varie stringhe dalla lista
        try {
            BufferedWriter writer = Files.newBufferedWriter(Path.of("Streams/CopiaFile/copia.txt"));

            for (String s : readLines) {
                writer.write(s + "\n");
            }
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
