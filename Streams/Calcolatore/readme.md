# Calcolatore
- prendere un file di testo in input
- I file contengono un'operazione aritmetica nel seguente formato
  - 2 2 +
  - 2 2 + 1 +
  - 2 2 + 1 + 4 -
- Il programma calcola il risultato e lo scrive su un file
  - 4
  - 5
  - 1