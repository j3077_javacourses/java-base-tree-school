import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;


public class Calcolatore {
    public static void main(String[] args) throws IOException {

        //Lettura file
        ArrayList<String> lines = new ArrayList<>();
        try {
            BufferedReader reader = Files.newBufferedReader(Path.of("Streams/Calcolatore/input.txt"));
            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        //operazioni e salvataggio dei risultati nella lista "results"
        ArrayList<Integer> results = new ArrayList<>();
        for (String s : lines) {
            String[] array = s.split(" ");
            int sum = Integer.parseInt(array[0]);
            for (int i=1; i<array.length; i+=2) {
                int num = Integer.parseInt(array[i]);
                String op = array[i+1];
                switch(op) {
                    case "+" -> sum += num;
                    case "-" -> sum -= num;
                }
            }
            results.add(sum);
        }

        //scrittura dei risultati sul nuovo file
        try {
            BufferedWriter writer = Files.newBufferedWriter(Path.of("Streams/Calcolatore/output.txt"));
            for (Integer i : results) {
                writer.write(i +"\n");
            }
            writer.close();
        }catch (IOException e ) {
            e.printStackTrace();
        }
    }
}


